package org.codeavenuetest.steps;


import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Sleeper;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;
import junit.framework.AssertionFailedError;

public class ToDoTest {
	
	private WebDriver driver;
	
	@Given("access to URL ToDo")
	public void access_to_URL_ToDo() {		
		System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chromedriver.exe");	
    	driver = new ChromeDriver();
    	driver.get("https://qa-test.avenuecode.com/");

	}

	@And("access login")
	public void accessLogin() {
		driver.findElement(By.linkText("Sign In")).click();
	}

	@And("input loginName")
	public void inputLoginName() {
		driver.findElement(By.id("user_email")).sendKeys("mortari7@hotmail.com");
	}

	@And("input password")
	public void inputPassword() {
		driver.findElement(By.id("user_password")).sendKeys("12345678");
	}
			
	@Then("confirm user access")
	public void confirmUserAccess() {
		driver.findElement(By.name("commit")).click();
	}		
	
	@Given("check link My Task")
	public void checkLinkMyTaskOnNavBar() {
		driver.findElement(By.linkText("My Tasks")).isDisplayed();
	}

	@Given("click Task")
	public void clickTask() {
		driver.findElement(By.linkText("My Tasks")).click();
	}

	@Then("click Home")
	public void clickHome() {
		driver.findElement(By.linkText("Home")).click();
	}
	
	@Then("confirm if welcom message is valid")
	public void confirmIfWelcomMessageIs() {	

		WebElement H1Element = driver.findElement(By.className("container"));
		String texto = H1Element.getText();
		String arg1 = "Hey Alessandro Mortari, this is your todo list for today:";		
		/*String arg1 = Alessandro Mortari's ToDo List;*/
		Assert.assertTrue(texto.contains(arg1));		
	}
	
	/* Create a task*/	
	@Given("create task")
	public void clickCreateTask() {
		driver.findElement(By.id("new_task")).sendKeys("Teste1");
		driver.findElement(By.id("new_task")).submit();
	}

	@Then("confirm task")
	public void confirmTask() {
		String teste = "Teste1";
		String task = driver.findElement(By.className("task_body")).getText();
		Assert.assertEquals(teste, task);
	}
	
	/* Create Subtask*/
	@Given("click the button of subtasks")
	public void clickTheButtonOfSubtasks() {
		driver.findElement(By.className("btn-primary")).click();
	}

	@Given("type subtask")
	public void typeSubtask() {
		driver.findElement(By.id("new_sub_task")).sendKeys("Teste1_sub");	
	}
	
	@Then("click in add button")
	public void clickInAddButtonToAddANewSubtask() {
		driver.findElement(By.id("add-subtask")).click();
	}

	@Given("subtask close")
	public void subtaskClose() {
		driver.findElement(By.id("add-subtask")).sendKeys(Keys.ESCAPE);
	}

	@Given("check if the button count the number of subtasks")
	public void checkIfTheButtonCountTheNumberOfSubtasks() {
		/*String texto = driver.findElement(By.className("btn-primary")).getText();
		String arg1 = "1";
		Assert.assertTrue(texto.contains(arg1));*/		
	}

	@Then("check if open modal of Subtasks")
	public void checkIfOpenModalOfSubtasks() {		
		Assert.assertTrue(driver.findElement(By.className("modal-body")).isDisplayed());		
	}	

	@Then("check if theres a check id field and check description")
	public void checkIfTheresACheckIdFieldAndCheckDescription() {
		Assert.assertTrue(driver.findElement(By.id("edit_task")).isDisplayed());		
		Assert.assertTrue(driver.findElement(By.id("new_sub_task")).isDisplayed());
	}	

	@Then("verify if new subtask has description and date fields")
	public void verifyIfNewSubtaskHasDescriptionAndDateFields() {		
		Assert.assertTrue(driver.findElement(By.id("new_sub_task")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.id("dueDate")).isDisplayed());
	}

	@Then("subtasks added must appear on the bottom of the modal")
	public void subtasksAddedMustAppearOnTheBottomOfTheModal() {
		
	}
	
	@Then("type date task")
	public void typeDateTask() {	 
	    driver.findElement(By.id("dueDate")).sendKeys("1/2/2019");
	}
	
	@Then("delete Task")
	public void deleteTask() {
		driver.findElement(By.className("btn-danger")).click();
	}

	@After
	public void closeApp() {
		driver.quit();
	}
	
}
