package org.codeavenuetest.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/report-html"},
		features="src/test/java/org/codeavenuetest/features/todoteste.feature",
		glue="org.codeavenuetest.steps",
		monochrome=false,
		snippets = SnippetType.CAMELCASE,
		dryRun=false,
		strict=false)
public class RunnerTest {	
	
}	


