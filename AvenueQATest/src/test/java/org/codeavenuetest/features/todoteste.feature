Feature: ToDoApp 
	As a ToDo App user
	I should be able to create a task
	So I can manage my tasks

Background: 
	Given access to URL ToDo
	And access login
	And input loginName
	And input password
	Then confirm user access
	
Scenario: Check link on NavBar	
	Given check link My Task
	And click Task
	Then check link My Task
	And click Home
	Then check link My Task
	
Scenario: Check Welcome Message
	Given click Task
	Then confirm if welcom message is valid
	
Scenario: Create a Task
	Given click Task
	And create task
	Then confirm task		
	
Scenario: Create subtask
	Given click Task
	And check if the button count the number of subtasks
	And click the button of subtasks
	Then check if open modal of Subtasks
	And check if theres a check id field and check description
	And type subtask
	And type date task
	And click in add button
	Then subtasks added must appear on the bottom of the modal
	And subtask close		
		
Scenario: Clear task
	Given click Task
	Then delete Task


