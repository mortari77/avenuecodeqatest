package org.cucumber.utils;

import java.util.Calendar;
import java.util.Date;

public class Dateutils {

	public static Date obterDataDiferenciaDias(int dias) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, dias);
		return cal.getTime();
	}
}
