package org.cucumber.entidade.servico;

import java.util.Calendar;

import org.cucumber.entidade.Filme;
import org.cucumber.entidade.NotaAlguel;

public class AluguelService {

	public NotaAlguel alugar(Filme filme) {
	
		NotaAlguel nota = new NotaAlguel();
		nota.setPreco(filme.getAluguel());
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 1);		
		nota.setDataEntrega(cal.getTime());
		filme.setEstoque(1);
		return nota;
	}
}
